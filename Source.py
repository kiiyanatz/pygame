import pygame
import time
import random

pygame.init()

display_h = 720
display_w = 400

black = (0, 0, 0)
white = (255, 255, 255)
red = (200, 0, 0)
green = (0, 200, 0)
blue = (0, 0, 255)

bright_red = (255, 0, 0)
bright_green = (0, 255, 0)

game_display = pygame.display.set_mode((display_w, display_h))

pygame.display.set_caption('NatzRacer')

clock = pygame.time.Clock()

# Clock (fps)
clock = pygame.time.Clock()

player_width = 50
player_height = 107

# Load player image
player_img = pygame.image.load('resources/imgs/player.png')


def avoided(count):
    font = pygame.font.SysFont(None, 25)
    text = font.render("Dodged: "+str(count), True, (255, 100, 200))
    game_display.blit(text, (0, 0))


def show_points(points):
    font = pygame.font.SysFont(None, 25)
    text = font.render("Points: "+str(points), True, (255, 100, 100))
    game_display.blit(text, (0, 20))


def block(block_x, block_y, block_w, block_h, color, count):
    # Draw box
    pygame.draw.rect(game_display, color, [block_x, block_y, block_w, block_h])


def player(x, y):
    game_display.blit(player_img, (x, y))


def text_objects(msg, font):
    text_surf = font.render(msg, True, black)
    return text_surf, text_surf.get_rect()


def message_display(msg, score):
    large_text = pygame.font.Font('freesansbold.ttf', 50)
    text_surf, text_rect = text_objects(msg, large_text)
    text_rect.center = ((display_w/2), (display_h/2))

    font = pygame.font.SysFont(None, 40)
    text = font.render("Highscore: "+str(score), True, red)
    game_display.blit(text, ((display_w/2), (display_h/2+100)))

    game_display.blit(text_surf, text_rect)

    pygame.display.update()

    time.sleep(2)

    game_loop()


def crash(score):
    message_display("You Crashed!", score)


def button(btn_x, btn_y, btn_w, btn_h, btn_text, btn_color, action=None):
    mouse = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()

    if btn_x+btn_w > mouse[0] > btn_x and btn_y+btn_h > mouse[1] > btn_y:
        if btn_color == green:
            pygame.draw.rect(game_display, bright_green, (btn_x, btn_y, btn_w, btn_h))
            if click[0] == 1 and action != None:
                if action == "play":
                    game_loop()
                else:
                    pass
        elif btn_color == red:
            pygame.draw.rect(game_display, bright_red, (btn_x, btn_y, btn_w, btn_h))
            if click[0] == 1 and action != None:
                if action == "quit":
                    pygame.quit()
                    quit()
                else:
                    pass
    else:
        pygame.draw.rect(game_display, btn_color, (btn_x, btn_y, btn_w, btn_h))

    small_text = pygame.font.Font("freesansbold.ttf", 20)
    text_surf, text_rect = text_objects(btn_text, small_text)
    text_rect.center = ((btn_x+(btn_w/2)), (btn_y+(btn_h/2)))
    game_display.blit(text_surf, text_rect)


def game_intro():
    intro = True

    while intro:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_g:
                    intro = False

        game_display.fill(white)
        large_text = pygame.font.Font('freesansbold.ttf', 30)
        text_surf, text_rect = text_objects("Welcome to NatzRacer", large_text)
        text_rect.center = ((display_w/2), (display_h-600))
        game_display.blit(text_surf, text_rect)

        button(100, 200, 200, 50, "Play", green, "play")
        button(100, 300, 200, 50, "Quit", red, "quit")

        pygame.display.update()
        clock.tick(15)

def game_loop():

    player_x = display_w * 0.45
    player_y = display_h * 0.8

    x_change = 0
    y_change = 0

    block_startx = random.randrange(0, display_w)
    block_starty = -600
    block_speed = 4
    block_w = 50
    block_h = 50

    block_count = 1

    dogded = 0

    points = 0

    game_exit = False

    while not game_exit:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    pygame.quit()
                    quit()
                if event.key == pygame.K_LEFT:
                    x_change = -5
                if event.key == pygame.K_RIGHT:
                    x_change = 5
                if event.key == pygame.K_UP:
                    y_change = -5
                if event.key == pygame.K_DOWN:
                    y_change = 5
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                    x_change = 0
                if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                    y_change = 0

        player_x += x_change
        player_y += y_change

        game_display.fill(white)
        block_starty += block_speed

        # Draw block
        block(block_startx, block_starty, block_w, block_h, red, block_count)


        # Draw player car
        player(player_x, player_y)

        # Count score
        avoided(dogded)

        # Poins
        show_points(points)


        # Collison logic
        if player_x > display_w-player_width or player_x < 0:
            crash(points)


        if block_starty > display_h:
            block_starty = 0 - block_h
            block_startx = random.randrange(0, display_w)
            dogded += 1
            block_speed += 1
            block_count += 1
            points = dogded * 15

        if player_y < block_starty + block_h:
            if player_x > block_startx and player_x < block_startx + block_w or player_x+player_width > block_startx and player_x + player_width < block_startx + block_w:
                crash(points)


        pygame.display.update()
        clock.tick(100)

game_intro()
game_loop()

pygame.quit()
quit()